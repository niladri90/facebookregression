package com.facebook.test;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class MethodRepository {

	WebDriver driver;

	public void appLaunch() {

		ChromeOptions options = new ChromeOptions();

		options.addArguments("--disable-notifications");

		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");

		driver = new ChromeDriver(options);

		driver.manage().window().maximize();

		driver.get("https://www.facebook.com/");
	}

	public void selectDob() {
		
		WebElement bdaydropdown = driver.findElement(By.name("birthday_day"));
		Select bday = new Select(bdaydropdown);
		bday.selectByVisibleText("6");
		
		WebElement bmonthdropdown = driver.findElement(By.name("birthday_month"));
		Select bmonth = new Select(bmonthdropdown);
		bmonth.selectByIndex(10);
		
		WebElement byeardropdown = driver.findElement(By.name("birthday_year"));
		Select byear = new Select(byeardropdown);
		byear.selectByValue("1990");
		
		

		//Select departingfrom = new Select(dropdown);

		//departingfrom.selectByVisibleText("London");
	}

	public void verifyLogin() throws AWTException, InterruptedException, FindFailed {
		//WebElement email = driver.findElement(By.xpath("//input[@name='email']"));

		//email.sendKeys("");

		/*String username = "niladripaul90@gmail.com";

		StringSelection usernameselection = new StringSelection(username);
		Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
		clipboard.setContents(usernameselection, usernameselection);

		Robot robot = new Robot();
		robot.keyPress(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_CONTROL);*/
		
	//Using javascriptexecutor	
		
		/*WebElement email = driver.findElement(By.xpath("//input[@name='email']"));	
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].value='niladripaul90@gmail.com';", email);
		
		 WebElement password = driver.findElement(By.xpath("//input[@name='pass']"));
		 
		 js.executeScript("arguments[0].value='babin90';", password);
		 
		 WebElement btnlogin = driver.findElement(By.xpath("//input[@value='Log In']"));
		 js.executeScript("arguments[0].click();",btnlogin);*/
		
		Thread.sleep(3000);
		
		/*WebElement email = driver.findElement(By.xpath("//input[@name='email']"));
		Actions username = new Actions(driver);
		username.moveToElement(email).click().sendKeys("niladripaul90@gmail.com").build().perform();*/
		
		Screen screen = new Screen();
		Pattern username = new Pattern("./images/Username1_fb.png");
		//screen.wait(username,10);
		//screen.type(username,"dasd");
		screen.wait(username,10);
		screen.type(username,"niladripaul90@gmail.com");
		
		Pattern password = new Pattern("./images/Password_fb.png");
		screen.wait(password,10);
		screen.type(password,"babin90");
		//Thread.sleep(3000);
		/*WebElement password = driver.findElement(By.xpath("//input[@name='pass']"));
		Actions pwd = new Actions(driver);
		pwd.moveToElement(password).click().sendKeys("babin90").build().perform();*/

		 WebElement login = driver.findElement(By.xpath("//input[@value='Log In']"));
		 Actions btnsignin = new Actions(driver);
		 btnsignin.moveToElement(login).click().build().perform();

		// WebElement password = driver.findElement(By.xpath("//input[@name='pass']"));

		// password.sendKeys("");
		
		/*Robot robot = new Robot();

		robot.keyPress(KeyEvent.VK_TAB);
		robot.keyRelease(KeyEvent.VK_TAB);

		robot.keyPress(KeyEvent.VK_B);
		robot.keyRelease(KeyEvent.VK_B);
		robot.keyPress(KeyEvent.VK_A);
		robot.keyRelease(KeyEvent.VK_A);
		robot.keyPress(KeyEvent.VK_B);
		robot.keyRelease(KeyEvent.VK_B);
		robot.keyPress(KeyEvent.VK_I);
		robot.keyRelease(KeyEvent.VK_I);
		robot.keyPress(KeyEvent.VK_N);
		robot.keyRelease(KeyEvent.VK_N);
		robot.keyPress(KeyEvent.VK_9);
		robot.keyRelease(KeyEvent.VK_9);
		robot.keyPress(KeyEvent.VK_0);
		robot.keyRelease(KeyEvent.VK_0);*/

		// password.sendKeys(robot);

		 
		// btnlogin.click();
		//Robot robot = new Robot();
		//robot.keyPress(KeyEvent.VK_ENTER);
		//robot.keyRelease(KeyEvent.VK_ENTER);

		String expTitle = "(2) Facebook";

		String actualtitle = driver.getTitle();

		System.out.println(actualtitle);

		if (expTitle.equals(actualtitle)) {

			System.out.println("Login Successfully");

		}

		else {

			System.out.println("Login Error");

		}

		// driver.close();

	}

	public void addPhoto() throws InterruptedException, IOException {

		WebElement btnprofilepic = driver.findElement(By.xpath("//a[@title='Profile']"));
		btnprofilepic.click();
		Thread.sleep(3000);

		List<WebElement> btnphoto = driver.findElements(By.linkText("Photos"));
		System.out.println(btnphoto.size());
		btnphoto.get(0).click();

		Thread.sleep(5000);
		WebElement btnaddphoto = driver.findElement(By.linkText("Add Photos/Video"));
		btnaddphoto.click();
		
		Runtime.getRuntime().exec("./Photoupload/Facebookphoto.exe");

	}



	public void settings() throws InterruptedException {
		// TODO Auto-generated method stub
		WebElement arrowicon = driver.findElement(By.xpath("//div[@id='userNavigationLabel']"));
		arrowicon.click();
		
		Thread.sleep(5000);
		WebElement settings = driver.findElement(By.xpath("//span[contains(text(),'Settings')]"));
		Actions rightclick = new Actions(driver);
		rightclick.contextClick(settings).click().build().perform();
	}

	
}
